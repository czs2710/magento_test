<?php

declare(strict_types=1);

namespace Magento\CustomModule\Model;
use Magento\Framework\Model\AbstractModel;

class CustomData
{
    public function getName(): string
    {
        return 'Custom Name';
    }
}
