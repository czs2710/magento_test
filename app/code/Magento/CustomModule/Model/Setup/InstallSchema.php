<?php

namespace Magento\CustomModule\Model\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()
            ->newTable($setup->getTable('custom_sample_item'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['primary' => true, 'identity' => true, 'nullable' => false],
            )
            ->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Item name'
            )->addIndex(
                $setup->getIdxName('custom_sample_item', ['name']),
                ['name']
            )->setComment(
                'Sample Items'
            );
        $setup->getConnection()->createTable($table);
        
        $setup->endSetup();
    }
}
