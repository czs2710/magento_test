<?php

declare(strict_types=1);

namespace Magento\CustomModule\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Index implements HttpGetActionInterface
{

    public function __construct(
        private PageFactory $pageFactory,
        private ScopeConfigInterface $scopeConfig,
    ){
    }

    public function execute(): Page 
    {
        $value = $this->scopeConfig->getValue('custom_section/custom_data/name');
        return $this->pageFactory->create();
        // die('Jumpstart');
    }
}
