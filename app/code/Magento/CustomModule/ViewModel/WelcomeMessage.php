<?php

declare(strict_types=1);

namespace Magento\CustomModule\ViewModel;

use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class WelcomeMessage implements ArgumentInterface
{
    public function __construct(
        private ScopeConfigInterface $scopeConfig,
    ){
    }

    public function getWelcomeMessage(): Phrase
    {
        $hour = date('G');

        if($hour < 12){
            return __('Good Morning');
        }if($hour < 12){
            return __('Good afternoon');
        }else{
            return __('Good night');
        }
    }

    public function getConfigMessage(): Phrase
    {
        $value = $this->scopeConfig->getValue('custom_section/custom_data/name');
        return __($value);
    }

    public function getConfigMessage2(): Phrase
    {
        $value = $this->scopeConfig->getValue('custom_section/custom_data/description');
        return __($value);
    }

}
